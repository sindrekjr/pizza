const availableToppings = [
    {
        name: 'Cheese',
        price: 30,
    },
    {
        name: 'Mushroom',
        price: 50
    },
    {
        name: 'Pepperoni',
        price: 75
    },
    {
        name: 'Pineapple',
        price: 35
    }
];

// New Builder Class using the Module Design Pattern.
const PizzaBuilder = function (appId) {

    // References to reused DOM Elements.
    const elApp      = document.getElementById(appId);
    const elPizza    = elApp.children.namedItem('pizza-container').children.namedItem('pizza');
    const elToppings = elApp.children.namedItem('toppings');
    const elToppingActions = elToppings.children.namedItem('topping--actions');
    const elOrder    = elApp.children.namedItem('order');
    const elOrderTotal = document.getElementById('order--total');
    const elOrderToppings = document.getElementById('order--toppings');

    // Verify child elements.
    if (!elPizza || !elToppings || !elOrder) {
        throw new Error("Please ensure you have elements with id's 'pizza', 'toppings' and 'order' inside your app element.");
    }

    // A list of toppings currently on the pizza.
    let currentToppings = [];

    // Helper Objects - IIFE Design Pattern
    const orderManager = (function() {

        let total = 0;

        return {
            update() {
                total = 80; 
                currentToppings.forEach(topping => total += topping.price);
                elOrderTotal.innerHTML = total; 
                this.updateToppingList(); 
            },
            updateToppingList() {
                elOrderToppings.innerHTML = '';

                if(currentToppings.length) {
                    currentToppings.forEach(topping => {
                        elOrderToppings.append(Object.assign(
                            document.createElement('li'),
                            {
                                innerHTML: `${topping.name} - ${topping.price}`
                            }
                        ));
                    });
                } else {
                    elOrderToppings.innerHTML = 'No toppings added.';
                }               
            }
        }
    })();
    const toppingManager = (function () {
        return {
            hasTopping(topping) {
                return currentToppings.find(t => t === topping); 
            },
            add(topping) {
                elPizza.append(Object.assign(
                    document.createElement('svg'),
                    {
                        classList: `topping topping--${topping.name.toLowerCase()}`
                    }
                )); 
                currentToppings.push(topping); 
            },
            remove(topping) {
                document.querySelector(`svg.topping--${topping.name.toLowerCase()}`).remove(); 
                currentToppings = currentToppings.filter(t => t != topping);
            }
        }
    }());

    return {
        /**
         * Create a basic Pizza Base
         * @returns {Builder}
         */
        createBase() {
            elPizza.appendChild(Object.assign(
                document.createElement('div'),
                {
                    classList: 'pizza-base'
                }
            ));
            orderManager.update(); 
            return this;
        },
        /**
         * Create the actions for all the toppings in the Topping list.
         * @returns {Builder}
         */
        createToppingActions() {
            for(let topping of availableToppings) {
                let btn = Object.assign(
                    document.createElement('button'),
                    {
                        classList: 'btn btn-topping',
                        innerHTML: topping.name,
                        onclick: () => this.toggleTopping(topping)
                    }
                )
                elToppingActions.append(btn); 
            }

            return this;
        },
        /**
         * Add or Remove a topping.
         * Uses the toppingManager object.
         * @param topping
         */
        toggleTopping(topping) {
            if(currentToppings.includes(topping)) {
                toppingManager.remove(topping); 
            } else {
                toppingManager.add(topping); 
            }

            orderManager.update(); 
        }
    }
};
